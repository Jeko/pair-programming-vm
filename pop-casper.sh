#! /usr/bin/env bash

#set -ex

readonly PROGNAME=$(basename "$0")
readonly ARGS=$@
readonly LOCAL_IP_ADDRESS=$(ip route get 8.8.8.8 | grep -oP 'src \K[^ ]+')
readonly EXTERNAL_IP_ADDRESS=$(curl ifconfig.me/ip)
readonly VM_TYPE_SHARED="shared"
readonly VM_TYPE_STANDALONE="standalone"
readonly VM_MEMORY_DEFAULT="8G"
VM_MEMORY=$VM_MEMORY_DEFAULT

main () {
    if [ -z "$ARGS" ]
    then
	usage
	exit
    else
	cmdline "$ARGS"
    fi
    attach_to_vm    
}

usage() {
    cat <<- EOF
	usage: $PROGNAME OPTIONS
	
	Setup a Guix System VM to host remote pair-programming sessions using Emacs and Free Software.
	
	OPTIONS:
	   -f --standalone          run a standalone VM with its own store
	   -s --shared-store        run a VM sharing its store with the host system
	   -h --help                show this help
	   -m --memory		    set dedicated memory for the VM default to 8G
	
	Examples:
	   Run a small VM with read-only store:
	   $PROGNAME --shared-store
	
	   Run a standalone VM with its own store:
	   $PROGNAME --standalone

	   Run a small VM with read-only store, provided with 4G of memory:
	   $PROGNAME --shared-store --memory 4G
	EOF
}

cmdline () {
    local arg=
    for arg
    do
	local delim=""
	case "$arg" in
	    --help)             args="${args}-h ";;
	    --standalone)       args="${args}-f ";;
	    --shared-store)     args="${args}-s ";;
	    --memory)           args="${args}-m ";;
	    *) [[ "${arg:0:1}" == "-" ]] || delim="\""
	       args="${args}${delim}${arg}${delim} ";;
	esac
    done
    eval set -- $args
    while getopts "hfsm:" OPTION
    do
	case $OPTION in
	    h)
		usage
		exit
		;;
	    m)
		VM_MEMORY=$OPTARG
		;;
	    f)
		readonly VM_TYPE=$VM_TYPE_STANDALONE
		;;
	    s)
		readonly VM_TYPE=$VM_TYPE_SHARED
		;;
	esac
    done
    return 0
}

attach_to_vm () {
    launch_vm
    wait_for_vm_to_be_ready
    display_instructions
    ssh_to_vm
}

launch_vm () {
    if [[ $VM_TYPE == $VM_TYPE_SHARED ]]
    then
	launch_shared_vm
    fi
    
    if [[ $VM_TYPE == $VM_TYPE_STANDALONE ]]
    then
	launch_standalone_vm
    fi
}

wait_for_vm_to_be_ready () {
    while ! ssh -o StrictHostKeyChecking=no -i id_rsa -p 10022 root@"${LOCAL_IP_ADDRESS}" ls 2> /dev/null; do
	sleep 1;
    done
}

display_instructions () {
    cat <<- EOF
    	Dear host,
	You will find the private key to authenticate to your VM using SSH here: $(find `pwd` -name id_rsa)
        Send it to your guests and remind them of setting the key permissions to 600.
	To connect from inside of your LAN, please do:
        ssh -o StrictHostKeyChecking=no -i id_rsa -p 10022 root@${LOCAL_IP_ADDRESS} -Y
	To connect from outside of your LAN, please do:
        ssh -Y -o StrictHostKeyChecking=no -i id_rsa -p 52222 root@${EXTERNAL_IP_ADDRESS}
	If it is the first time you connect to the VM, you might be interested in running the script setup-tools.sh !
        I wish you, and your guests, a very happy hacking session.
        Kind regards,
	EOF
}

ssh_to_vm () {
    ssh -Y -o StrictHostKeyChecking=no -i id_rsa -p 10022 root@"${LOCAL_IP_ADDRESS}"
}

launch_shared_vm () {
    if ! an_image_exists
    then
	renew_ssh_keys
    fi
    $(guix system vm pair-programming.scm) -m "$VM_MEMORY" -net nic -net user,hostfwd=tcp:"${LOCAL_IP_ADDRESS}":10022-:22 &
}

launch_standalone_vm () {
    if an_image_exists
    then
	launch_existing_vm
    else
	launch_new_vm
    fi
}

an_image_exists () {
    local vm=$(ls -- *qcow2)
    if [ -f "$vm" ]
    then
	return 0
    else
	return 1
    fi
}

launch_existing_vm () {
    local vm=$(ls -- *qcow2)
    guix environment --ad-hoc qemu -- qemu-system-x86_64 -enable-kvm -device virtio-blk,drive=guix -drive if=none,id=guix,file="$vm" -m "$VM_MEMORY" -net nic -net user,hostfwd=tcp:"${LOCAL_IP_ADDRESS}":10022-:22 &
}

launch_new_vm () {
    renew_ssh_keys
    create_image
    launch_existing_vm
}

create_image () {
    casper_image=$(guix system image --image-size=10G --image-type=qcow2 pair-programming.scm )
    cp "$casper_image" .
    copy=$(basename "$casper_image")
    chmod +w "$copy"
    echo "$copy"
}

renew_ssh_keys () {
    ssh-keygen -R ["${LOCAL_IP_ADDRESS}"]:10022
    ssh-keygen -R ["${EXTERNAL_IP_ADDRESS}"]:52222
    rm id_rsa*
    ssh-keygen -f id_rsa -P ''
    chmod 600 id_rsa*
}

main
