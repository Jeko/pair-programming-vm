(use-modules (gnu)
	     (jeko-packages))

(use-service-modules networking
		     ssh)

(use-package-modules ssh
		     emacs
		     emacs-xyz
		     guile
		     guile-xyz
		     version-control
		     certs
		     code
		     haskell-apps
		     fonts)

(operating-system
 (host-name "casper")
 (timezone "Europe/Paris")
 (locale "fr_FR.utf8")
 (bootloader (bootloader-configuration
	      (bootloader grub-bootloader)
	      (target "/dev/vda")))
 (file-systems (cons (file-system
		      (device "/dev/vda1")
		      (mount-point "/")
		      (type "ext4"))
                     %base-file-systems))
 
 (packages (cons* emacs
		  emacs-use-package
		  emacs-which-key
		  emacs-doom-modeline
		  emacs-minions
		  emacs-flymake-shellcheck
		  emacs-paredit
		  emacs-yasnippet
		  emacs-iedit
		  emacs-guix
		  emacs-flycheck
		  emacs-flycheck-guile
		  emacs-geiser
		  emacs-multiple-cursors
		  emacs-counsel-projectile
		  emacs-magit
		  emacs-ggtags
		  emacs-projectile
		  emacs-paren-face
		  emacs-aggressive-indent
		  emacs-web-mode
		  emacs-wgrep
		  emacs-ag
		  emacs-counsel
		  emacs-swiper
		  emacs-ivy-rich
		  emacs-company
		  emacs-company-quickhelp
		  emacs-grep-context
		  emacs-debbugs
		  emacs-keycast
		  emacs-gif-screencast
		  emacs-flycheck-grammalecte
		  emacs-perspective
		  
		  shellcheck
		  
		  guile-readline
		  guile-colorized
		  guile-hall
		  guile-spec
		  guile-studio
		  
		  git
		  nss-certs
		  
		  the-silver-searcher
		  
		  font-jetbrains-mono
		  %base-packages))

 (services
  (append
   (list
    (service dhcp-client-service-type)
    (service openssh-service-type
             (openssh-configuration
	      (x11-forwarding? #t)
	      (permit-root-login 'without-password)
	      (authorized-keys
	       `(("root" ,(local-file "id_rsa.pub"))))
	      (port-number 22)))
    (extra-special-file "/root/setup-tools.sh"
			(local-file "setup-tools.sh"))
    (simple-service '256-color-term-env session-environment-service-type
                    '(("TERM" . "xterm-256color"))))
   %base-services)))
