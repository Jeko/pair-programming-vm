#!/usr/bin/env bash

# Download guile-spec script to automate testing
wget https://framagit.org/Jeko/guile-spec/-/raw/master/examples/spcw.sh

# Setup Emacs configuration
git clone https://framagit.org/Jeko/emacs-config.git .emacs.d

# Setup Guix sources
git clone https://framagit.org/Jeko/guix.git
(
    cd guix || exit
    guix environment guix --pure -- ./bootstrap \
	&& guix environment guix --pure -- ./configure --localstatedir=/var \
	&& guix environment guix --pure -- make
)

echo "Workspace ready. Have fun !"
